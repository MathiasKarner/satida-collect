-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:14:1
	package
		ADDED from AndroidManifest.xml:15:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:17:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		ADDED from AndroidManifest.xml:16:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:14:11
uses-feature#android.hardware.location
ADDED from AndroidManifest.xml:19:5
	android:required
		ADDED from AndroidManifest.xml:21:9
	android:name
		ADDED from AndroidManifest.xml:20:9
uses-feature#android.hardware.location.network
ADDED from AndroidManifest.xml:22:5
	android:required
		ADDED from AndroidManifest.xml:24:9
	android:name
		ADDED from AndroidManifest.xml:23:9
uses-feature#android.hardware.location.gps
ADDED from AndroidManifest.xml:25:5
	android:required
		ADDED from AndroidManifest.xml:27:9
	android:name
		ADDED from AndroidManifest.xml:26:9
uses-feature#android.hardware.telephony
ADDED from AndroidManifest.xml:28:5
	android:required
		ADDED from AndroidManifest.xml:30:9
	android:name
		ADDED from AndroidManifest.xml:29:9
uses-feature#android.hardware.wifi
ADDED from AndroidManifest.xml:31:5
	android:required
		ADDED from AndroidManifest.xml:33:9
	android:name
		ADDED from AndroidManifest.xml:32:9
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:35:5
	android:name
		ADDED from AndroidManifest.xml:35:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:36:5
	android:name
		ADDED from AndroidManifest.xml:36:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:37:5
	android:name
		ADDED from AndroidManifest.xml:37:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:38:5
	android:name
		ADDED from AndroidManifest.xml:38:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:39:5
	android:name
		ADDED from AndroidManifest.xml:39:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:40:5
	android:name
		ADDED from AndroidManifest.xml:40:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:41:5
	android:name
		ADDED from AndroidManifest.xml:41:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:42:5
	android:name
		ADDED from AndroidManifest.xml:42:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:43:5
	android:name
		ADDED from AndroidManifest.xml:43:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:44:5
	android:name
		ADDED from AndroidManifest.xml:44:22
permission#org.opendatakit.tables.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:54:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:56:9
	android:name
		ADDED from AndroidManifest.xml:55:9
uses-permission#org.opendatakit.tables.permission.MAPS_RECEIVE
ADDED from AndroidManifest.xml:58:5
	android:name
		ADDED from AndroidManifest.xml:58:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:59:5
	android:name
		ADDED from AndroidManifest.xml:59:22
uses-sdk
ADDED from AndroidManifest.xml:61:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:63:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:62:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
supports-screens
ADDED from AndroidManifest.xml:65:5
	android:largeScreens
		ADDED from AndroidManifest.xml:67:9
	android:smallScreens
		ADDED from AndroidManifest.xml:70:9
	android:normalScreens
		ADDED from AndroidManifest.xml:68:9
	android:xlargeScreens
		ADDED from AndroidManifest.xml:71:9
	android:resizeable
		ADDED from AndroidManifest.xml:69:9
	android:anyDensity
		ADDED from AndroidManifest.xml:66:9
application
ADDED from AndroidManifest.xml:73:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:76:9
	android:icon
		ADDED from AndroidManifest.xml:75:9
	android:largeHeap
		ADDED from AndroidManifest.xml:77:9
	android:theme
		ADDED from AndroidManifest.xml:78:9
	android:name
		ADDED from AndroidManifest.xml:74:9
provider#com.satida.collect.android.provider.FormsProvider
ADDED from AndroidManifest.xml:79:9
	android:authorities
		ADDED from AndroidManifest.xml:81:13
	android:exported
		ADDED from AndroidManifest.xml:82:13
	android:name
		ADDED from AndroidManifest.xml:80:13
provider#com.satida.collect.android.provider.InstanceProvider
ADDED from AndroidManifest.xml:83:9
	android:authorities
		ADDED from AndroidManifest.xml:85:13
	android:exported
		ADDED from AndroidManifest.xml:86:13
	android:name
		ADDED from AndroidManifest.xml:84:13
activity#com.satida.collect.android.activities.GeoODK
ADDED from AndroidManifest.xml:88:9
	android:label
		ADDED from AndroidManifest.xml:91:13
	android:configChanges
		ADDED from AndroidManifest.xml:90:13
	android:name
		ADDED from AndroidManifest.xml:89:13
activity#com.satida.collect.android.activities.MainMenuActivity
ADDED from AndroidManifest.xml:93:9
	android:label
		ADDED from AndroidManifest.xml:96:13
	android:configChanges
		ADDED from AndroidManifest.xml:95:13
	android:name
		ADDED from AndroidManifest.xml:94:13
activity#com.satida.collect.android.activities.FormEntryActivity
ADDED from AndroidManifest.xml:98:9
	android:label
		ADDED from AndroidManifest.xml:101:13
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:102:13
	android:configChanges
		ADDED from AndroidManifest.xml:100:13
	android:name
		ADDED from AndroidManifest.xml:99:13
intent-filter#android.intent.action.EDIT+android.intent.action.VIEW+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:103:13
action#android.intent.action.VIEW
ADDED from AndroidManifest.xml:104:17
	android:name
		ADDED from AndroidManifest.xml:104:25
action#android.intent.action.EDIT
ADDED from AndroidManifest.xml:105:17
	android:name
		ADDED from AndroidManifest.xml:105:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:107:17
	android:name
		ADDED from AndroidManifest.xml:107:27
data
ADDED from AndroidManifest.xml:109:17
	android:mimeType
		ADDED from AndroidManifest.xml:109:23
activity#com.satida.collect.android.activities.DrawActivity
ADDED from AndroidManifest.xml:113:9
	android:label
		ADDED from AndroidManifest.xml:115:13
	android:name
		ADDED from AndroidManifest.xml:114:13
activity#com.satida.collect.android.activities.InstanceChooserList
ADDED from AndroidManifest.xml:116:9
	android:label
		ADDED from AndroidManifest.xml:118:13
	android:name
		ADDED from AndroidManifest.xml:117:13
activity#com.satida.collect.android.activities.InstanceChooserTabs
ADDED from AndroidManifest.xml:129:9
	android:label
		ADDED from AndroidManifest.xml:131:13
	android:name
		ADDED from AndroidManifest.xml:130:13
activity#com.satida.collect.android.activities.FormChooserList
ADDED from AndroidManifest.xml:132:9
	android:label
		ADDED from AndroidManifest.xml:134:13
	android:name
		ADDED from AndroidManifest.xml:133:13
intent-filter#android.intent.action.EDIT+android.intent.action.PICK+android.intent.action.VIEW+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:135:13
action#android.intent.action.PICK
ADDED from AndroidManifest.xml:138:17
	android:name
		ADDED from AndroidManifest.xml:138:25
activity#com.satida.collect.android.activities.FormManagerList
ADDED from AndroidManifest.xml:145:9
	android:label
		ADDED from AndroidManifest.xml:147:13
	android:name
		ADDED from AndroidManifest.xml:146:13
activity#com.satida.collect.android.activities.FormDownloadList
ADDED from AndroidManifest.xml:148:9
	android:label
		ADDED from AndroidManifest.xml:150:13
	android:name
		ADDED from AndroidManifest.xml:149:13
activity#com.satida.collect.android.activities.DataManagerList
ADDED from AndroidManifest.xml:151:9
	android:label
		ADDED from AndroidManifest.xml:153:13
	android:name
		ADDED from AndroidManifest.xml:152:13
activity#com.satida.collect.android.activities.FileManagerTabs
ADDED from AndroidManifest.xml:154:9
	android:label
		ADDED from AndroidManifest.xml:156:13
	android:name
		ADDED from AndroidManifest.xml:155:13
activity#com.satida.collect.android.activities.InstanceUploaderList
ADDED from AndroidManifest.xml:157:9
	android:label
		ADDED from AndroidManifest.xml:159:13
	android:name
		ADDED from AndroidManifest.xml:158:13
activity#com.satida.collect.android.activities.InstanceUploaderActivity
ADDED from AndroidManifest.xml:167:9
	android:label
		ADDED from AndroidManifest.xml:169:13
	android:name
		ADDED from AndroidManifest.xml:168:13
activity#com.satida.collect.android.preferences.PreferencesActivity
ADDED from AndroidManifest.xml:170:9
	android:label
		ADDED from AndroidManifest.xml:172:13
	android:name
		ADDED from AndroidManifest.xml:171:13
activity#com.satida.collect.android.preferences.AdminPreferencesActivity
ADDED from AndroidManifest.xml:173:9
	android:label
		ADDED from AndroidManifest.xml:175:13
	android:name
		ADDED from AndroidManifest.xml:174:13
activity#com.satida.collect.android.preferences.MapSettings
ADDED from AndroidManifest.xml:176:9
	android:label
		ADDED from AndroidManifest.xml:178:13
	android:name
		ADDED from AndroidManifest.xml:177:13
activity#com.satida.collect.android.activities.FormHierarchyActivity
ADDED from AndroidManifest.xml:179:9
	android:label
		ADDED from AndroidManifest.xml:181:13
	android:name
		ADDED from AndroidManifest.xml:180:13
activity#com.satida.collect.android.activities.GeoPointActivity
ADDED from AndroidManifest.xml:182:9
	android:label
		ADDED from AndroidManifest.xml:184:13
	android:name
		ADDED from AndroidManifest.xml:183:13
activity#com.satida.collect.android.activities.GeoPointMapActivity
ADDED from AndroidManifest.xml:185:9
	android:label
		ADDED from AndroidManifest.xml:187:13
	android:name
		ADDED from AndroidManifest.xml:186:13
activity#com.satida.collect.android.activities.GeoPointMapActivitySdk7
ADDED from AndroidManifest.xml:188:9
	android:label
		ADDED from AndroidManifest.xml:190:13
	android:name
		ADDED from AndroidManifest.xml:189:13
activity#com.satida.collect.android.activities.BearingActivity
ADDED from AndroidManifest.xml:191:9
	android:label
		ADDED from AndroidManifest.xml:193:13
	android:name
		ADDED from AndroidManifest.xml:192:13
activity#com.satida.collect.android.activities.SplashScreenActivity
ADDED from AndroidManifest.xml:194:9
	android:label
		ADDED from AndroidManifest.xml:196:13
	android:theme
		ADDED from AndroidManifest.xml:197:13
	android:name
		ADDED from AndroidManifest.xml:195:13
intent-filter#android.intent.action.MAIN+android.intent.category.DEFAULT+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:198:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:199:17
	android:name
		ADDED from AndroidManifest.xml:199:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:202:17
	android:name
		ADDED from AndroidManifest.xml:202:27
activity#com.satida.collect.android.activities.AndroidShortcuts
ADDED from AndroidManifest.xml:206:9
	android:label
		ADDED from AndroidManifest.xml:208:13
	android:theme
		ADDED from AndroidManifest.xml:209:13
	android:name
		ADDED from AndroidManifest.xml:207:13
intent-filter#android.intent.action.CREATE_SHORTCUT+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:210:13
action#android.intent.action.CREATE_SHORTCUT
ADDED from AndroidManifest.xml:211:17
	android:name
		ADDED from AndroidManifest.xml:211:25
receiver#com.satida.collect.android.receivers.NetworkReceiver
ADDED from AndroidManifest.xml:217:9
	android:enabled
		ADDED from AndroidManifest.xml:219:13
	android:name
		ADDED from AndroidManifest.xml:218:13
intent-filter#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:220:13
action#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:221:17
	android:name
		ADDED from AndroidManifest.xml:221:25
intent-filter#com.geoodk.collect.android.FormSaved
ADDED from AndroidManifest.xml:223:13
action#com.geoodk.collect.android.FormSaved
ADDED from AndroidManifest.xml:224:17
	android:name
		ADDED from AndroidManifest.xml:224:25
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:228:9
	android:value
		ADDED from AndroidManifest.xml:230:13
	android:name
		ADDED from AndroidManifest.xml:229:13
uses-library#com.google.android.maps
ADDED from AndroidManifest.xml:232:9
	android:required
		ADDED from AndroidManifest.xml:234:13
	android:name
		ADDED from AndroidManifest.xml:233:13
activity#com.satida.collect.android.activities.OSM_Map
ADDED from AndroidManifest.xml:236:9
	android:label
		ADDED from AndroidManifest.xml:239:13
	android:configChanges
		ADDED from AndroidManifest.xml:238:13
	android:name
		ADDED from AndroidManifest.xml:237:13
activity#com.satida.collect.android.activities.MainSettingsActivity
ADDED from AndroidManifest.xml:241:9
	android:label
		ADDED from AndroidManifest.xml:243:13
	android:name
		ADDED from AndroidManifest.xml:242:13
activity#com.satida.collect.android.activities.GeoShapeActivity
ADDED from AndroidManifest.xml:245:9
	android:label
		ADDED from AndroidManifest.xml:248:13
	android:configChanges
		ADDED from AndroidManifest.xml:247:13
	android:name
		ADDED from AndroidManifest.xml:246:13
activity#com.satida.collect.android.activities.GeoTraceActivity
ADDED from AndroidManifest.xml:250:9
	android:label
		ADDED from AndroidManifest.xml:253:13
	android:configChanges
		ADDED from AndroidManifest.xml:252:13
	android:name
		ADDED from AndroidManifest.xml:251:13
activity#com.satida.collect.android.activities.UpdateSatidaLayersActivity
ADDED from AndroidManifest.xml:255:9
	android:label
		ADDED from AndroidManifest.xml:258:13
	android:configChanges
		ADDED from AndroidManifest.xml:257:13
	android:name
		ADDED from AndroidManifest.xml:256:13
intent-filter#android.intent.category.DEFAULT+com.geoodk.collect.android.action.SATIDA_UPDATE_LAYERS
ADDED from AndroidManifest.xml:259:13
action#com.geoodk.collect.android.action.SATIDA_UPDATE_LAYERS
ADDED from AndroidManifest.xml:260:17
	android:name
		ADDED from AndroidManifest.xml:260:25
activity#com.satida.collect.android.activities.ChartActivity
ADDED from AndroidManifest.xml:265:9
	android:configChanges
		ADDED from AndroidManifest.xml:267:13
	android:name
		ADDED from AndroidManifest.xml:266:13
meta-data#com.google.android.gms.version
ADDED from com.google.android.gms:play-services:6.5.87:21:9
	android:value
		ADDED from com.google.android.gms:play-services:6.5.87:23:13
	android:name
		ADDED from com.google.android.gms:play-services:6.5.87:22:13
