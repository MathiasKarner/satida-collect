package com.satida.collect.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.Gson;
import com.satida.collect.android.R;
import com.satida.collect.android.spatial.TimelineData;
import com.satida.collect.android.utilities.ChartBase;
import com.satida.collect.android.utilities.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by MathiasKarner on 29.04.2015.
 */
public class ChartActivity extends ChartBase {

    private CombinedChart mChart;
    private TimelineData mData;

    private static boolean normalizeWarningLevels = true;
    private static int maxValsOnScreen = 40;

    private String[] dates = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeChartValues();
        initializeChart();

        draw();

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
    }

    private void initializeChart() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chart);

        mChart = (CombinedChart) findViewById(R.id.chart1);

        mChart.setDrawGridBackground(false);

        // mChart.setStartAtZero(true);

        // enable value highlighting
        mChart.setHighlightEnabled(true);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setVisibleXRange(maxValsOnScreen);
        mChart.setVisibleYRange(4, YAxis.AxisDependency.RIGHT);
        mChart.moveViewToX(mData.getCountValues() - maxValsOnScreen);
    }

    private void initializeChartValues() {
        Intent intent = getIntent();
        Gson gson = new Gson();
        dates = intent.getStringArrayExtra("dates");
        String json = intent.getStringExtra("data");
        mData = gson.fromJson(json, TimelineData.class);
        mData.calculateWarningThresholds();
        if (normalizeWarningLevels) mData.normalizeWarningLevels();
    }
    public void draw() {
        CombinedData data = new CombinedData(this.dates);

        data.setData(generateBarData());
        data.setData(generateLineData());

        mChart.setData(data);
        mChart.invalidate();

        CombinedChart.DrawOrder[] da = {CombinedChart.DrawOrder.BAR,CombinedChart.DrawOrder.LINE};
        ((CombinedChart)mChart).setDrawOrder(da);
    }

    private LineData generateLineData() {
        LineData d = new LineData();
        List<LineDataSet> datasets = mData.getWarningThresholds(true);
        for (LineDataSet ds : datasets) d.addDataSet(ds);
        return d;
    }

    private BarData generateBarData() {
        BarData b = new BarData();
        b.addDataSet(mData.getBarDataset());
        return b;
    }
}
