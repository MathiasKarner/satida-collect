package com.satida.collect.android.spatial;

import android.content.Context;
import android.view.MotionEvent;

import org.osmdroid.ResourceProxy;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.TilesOverlay;

/**
 * Created by karner on 22.04.2015.
 */
public class MBTilesOverlay extends TilesOverlay {
    private final OnClickListener mListener;

    public MBTilesOverlay(MapTileProviderBase aTileProvider, Context aContext, OnClickListener listener) {
        super(aTileProvider, aContext);
        mListener = listener;
    }

    public MBTilesOverlay(MapTileProviderBase aTileProvider, ResourceProxy pResourceProxy, OnClickListener listener) {
        super(aTileProvider, pResourceProxy);
        mListener = listener;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e, MapView mapView) {
        Projection proj = mapView.getProjection();
        IGeoPoint p = proj.fromPixels(((Float)e.getX()).intValue(), ((Float)e.getY()).intValue());
        if (mListener != null) mListener.onClickListener(p);
        return super.onSingleTapUp(e, mapView);
    }

    public interface OnClickListener{
        public void onClickListener(IGeoPoint point);
    }
}
