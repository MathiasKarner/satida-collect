package com.satida.collect.android.activities;

import com.satida.collect.android.R;
import com.satida.collect.android.adapters.MBTilesDownloadAdapter;
import com.satida.collect.android.application.Collect;
import com.satida.collect.android.utilities.MBTileDownloadManager;
import com.satida.collect.android.views.TwoItemMultipleProgressChoiceView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class UpdateSatidaLayersActivity extends Activity {

    private static final String TAG = "UpdateSatidaLayersActivity";
    private static final int REFRESH_LIST_DIALOG = 1;

    private MBTileDownloadManager downloadManager = null;
    private Integer resultReceiverID = null;
    private UpdateSatidaLayersResultReceiver resultReceiver = null;
    private MBTilesDownloadAdapter mbTilesDownloadAdapter = null;


    private ListView downloadItemList = null;
    private ProgressDialog progressDialog;

    public UpdateSatidaLayersActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_satida_layers);
        downloadManager = new MBTileDownloadManager();
        resultReceiver = new UpdateSatidaLayersResultReceiver(null);
        resultReceiverID = downloadManager.addResultReceiver(this.resultReceiver);
        initializeUI();
    }

    private void initializeUI() {
        Button btnToggle = ((Button)findViewById(R.id.toggle_button));
        Button btnRefresh = ((Button)findViewById(R.id.refresh_button));
        Button btnDownload = ((Button)findViewById(R.id.add_button));
        downloadItemList = (ListView) findViewById(R.id.listviewSATIDALayers);

        btnToggle.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(UpdateSatidaLayersActivity.this, "mooo", Toast.LENGTH_SHORT).show();
            }
        });
        btnRefresh.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                updateDatasetList();
            }
        });
        btnDownload.setOnClickListener(new OnClickListener() {            
            @Override
            public void onClick(View v) {
                downloadManager.download(((MBTilesDownloadAdapter)downloadItemList.getAdapter()).getItemsToDownload());
            }
        });
    }

    private void updateDatasetList(){
        showDialog(REFRESH_LIST_DIALOG);
        this.downloadManager.sendUpdateListRequest();
    }
/*
    private void loadCompletedDownloadList() {
        File file = new File(Collect.SATIDA_LAYER_JSON_PATH);
        final Type type = new TypeToken<List<MBTileDownloadDetails>>() {
        }.getType();
        if (file.exists()){          
            Gson gson = new Gson();
            completedDownloads = gson.fromJson(getStringFromFile(Collect.SATIDA_LAYER_JSON_PATH), type);
            
        }else{
            completedDownloads = new ArrayList<MBTileDownloadDetails>();
        }
    }
*/
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case REFRESH_LIST_DIALOG:
                Collect.getInstance().getActivityLogger().logAction(this, "onCreateDialog.PROGRESS_DIALOG", "show");
                progressDialog = new ProgressDialog(this);
                DialogInterface.OnClickListener loadingButtonListener =
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Collect.getInstance().getActivityLogger().logAction(this, "onCreateDialog.PROGRESS_DIALOG", "OK");
                            dialog.dismiss();
                        }
                    };
                    progressDialog.setTitle(getString(R.string.downloading_data));
                    progressDialog.setMessage(getString(R.string.downloading_data));
                    progressDialog.setIcon(android.R.drawable.ic_dialog_info);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setCancelable(false);
                    progressDialog.setButton(getString(R.string.cancel), loadingButtonListener);
                return progressDialog;
        }
        return null;
    }

    class UIActionUpdate implements Runnable {
        String msg = null;
        public UIActionUpdate(String message) {
            msg = message;
        }

        public void run() {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (msg!=null){
                Toast.makeText(UpdateSatidaLayersActivity.this, "Updating failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UILayerStatusUpdate implements Runnable {
        Integer result = null;
        String[] data = null;
        Integer downloadID = null;
        public UILayerStatusUpdate(Integer result, String data) {
            this.result = result;
            this.data = data.split(";");
            this.downloadID = Integer.parseInt(this.data[0]);
        }

        public void run() {
            TwoItemMultipleProgressChoiceView view = ((TwoItemMultipleProgressChoiceView)downloadItemList.findViewById(downloadID));
            switch (result) {
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_PROGRESS:
                    view.setProgress(Integer.parseInt(this.data[1]));
                    break;
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_SUCCEEDED:
                    view.setProgressBarVisibility(View.INVISIBLE);
                    view.setProgress(0);
                    Toast.makeText(UpdateSatidaLayersActivity.this, "Download finished", Toast.LENGTH_SHORT).show();
                    break;
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_FAILED:
                    view.setProgressBarVisibility(View.INVISIBLE);
                    view.setProgress(0);
                    Toast.makeText(UpdateSatidaLayersActivity.this, "Download failed", Toast.LENGTH_SHORT).show();
                    break;
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_STARTED:
                    view.setProgressBarVisibility(View.VISIBLE);
                    view.setProgress(0);
                    Toast.makeText(UpdateSatidaLayersActivity.this, "Download started", Toast.LENGTH_SHORT).show();
                    break;
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_CANCELED:
                    view.setProgressBarVisibility(View.INVISIBLE);
                    view.setProgress(0);
                    Toast.makeText(UpdateSatidaLayersActivity.this, "Download canceled", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }


    class UpdateSatidaLayersResultReceiver extends ResultReceiver {
        public UpdateSatidaLayersResultReceiver(final Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(final int resultCode, final Bundle resultData) {
            switch (resultCode) {
                case MBTileDownloadManager.RESULT_LIST_UPDATE_FAILED:
                    runOnUiThread(new UIActionUpdate("Updating list failed"));
                    break;
                case MBTileDownloadManager.RESULT_LIST_UPDATE_SUCCEEDED:
                    runOnUiThread(new UIActionUpdate(null));
                    mbTilesDownloadAdapter = new MBTilesDownloadAdapter(UpdateSatidaLayersActivity.this, downloadManager.getMergedList());
                    downloadItemList.setAdapter(mbTilesDownloadAdapter);
                    break;
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_SUCCEEDED:
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_FAILED:
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_PROGRESS:
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_STARTED:
                case MBTileDownloadManager.RESULT_LAYER_UPDATE_CANCELED:
                    runOnUiThread(new UILayerStatusUpdate(resultCode, resultData.getString("data")));
                    break;
            }
        }
    }
}
