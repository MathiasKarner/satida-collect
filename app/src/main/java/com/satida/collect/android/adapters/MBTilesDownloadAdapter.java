package com.satida.collect.android.adapters;

import java.util.ArrayList;
import java.util.List;

import com.satida.collect.android.R;
import com.satida.collect.android.activities.UpdateSatidaLayersActivity;
import com.satida.collect.android.spatial.MBTileDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class MBTilesDownloadAdapter extends BaseAdapter {
    private static String TAG = "MBTilesDownloadAdapter";
    private final UpdateSatidaLayersActivity ctx;
    private List<MBTileDetails> items = null;

    public MBTilesDownloadAdapter(final UpdateSatidaLayersActivity context, final List<MBTileDetails> mbTiles) {
        this.ctx = context;
        this.items = mbTiles;
    }

    @Override
    public int getCount() {return this.items.size();}

    @Override
    public MBTileDetails getItem(int position) {return items.get(position);}

    @Override
    public long getItemId(int position) {return items.get(position).getID();}

    public List<MBTileDetails> getItems(){return items;}

    public List<MBTileDetails> getItemsToDownload(){
        List<MBTileDetails> list  = new ArrayList<MBTileDetails>();
        for (MBTileDetails item:this.items) {
            if (item.getDownload()) list.add(item);
        }
        return list;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.two_item_multiple_progress_choice, parent, false);
        final TextView textView = (TextView) rowView.findViewById(R.id.text1);
        ((TextView) rowView.findViewById(R.id.text2)).setText("test1");
        ((TextView) rowView.findViewById(R.id.text3)).setText("test2");
        final MBTileDetails downloadItem = (MBTileDetails)this.getItem(position);
        rowView.setId(downloadItem.getID());
        textView.setText(downloadItem.getName());
        final CheckBox checkbox = (CheckBox) rowView.findViewById(R.id.checkbox);
        checkbox.setId(position);
        checkbox.setClickable(true);
        checkbox.setFocusable(true);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                final MBTileDetails downloadItem = MBTilesDownloadAdapter.this.items.get(buttonView.getId());
                downloadItem.setDownload(buttonView.isChecked());
            }
        });
        
        return rowView;
    }
}
