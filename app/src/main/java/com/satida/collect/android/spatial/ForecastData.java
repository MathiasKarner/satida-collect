package com.satida.collect.android.spatial;

/**
 * Created by karner on 29.04.2015.
 */
public class ForecastData {
    private Double[][][] forecastClasses;
    private Double nodata;
    private Double[][][] forecastProbability;

    public ForecastData(){}

    public Double[] getForecast(Integer posLat, Integer posLng) {
        return forecastClasses[posLat][posLng];
    }

    public Double[] getProbabilities(Integer posLat, Integer posLng) {
        return forecastProbability[posLat][posLng];
    }
}
