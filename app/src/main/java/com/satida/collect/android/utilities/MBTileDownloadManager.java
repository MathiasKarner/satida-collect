package com.satida.collect.android.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.satida.collect.android.application.Collect;
import com.satida.collect.android.preferences.MapSettings;
import com.satida.collect.android.spatial.MBTileDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;

import org.apache.http.Header;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by karner on 25.02.2015.
 */
public class MBTileDownloadManager {
    private static final String TAG = "MBTileDownloadManager";
    private HashMap<Integer, ResultReceiver> resultReceivers = new HashMap<Integer, ResultReceiver>();

    public static final int RESULT_LIST_UPDATE_FAILED = 1;
    public static final int RESULT_LIST_UPDATE_SUCCEEDED = 2;
    public static final int RESULT_LAYER_UPDATE_STARTED = 3;
    public static final int RESULT_LAYER_UPDATE_CANCELED = 4;
    public static final int RESULT_LAYER_UPDATE_SUCCEEDED = 5;
    public static final int RESULT_LAYER_UPDATE_FAILED = 6;
    public static final int RESULT_LAYER_UPDATE_PROGRESS = 7;

    private SharedPreferences sharedPreferences;
    private String serverURL;

    private List<MBTileDetails> availableDownloads = null;
    private List<MBTileDetails> completedDownloads = null;

    private List<MBTileDownloader> downloaders = new ArrayList<MBTileDownloader>();

    public MBTileDownloadManager(){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Collect.getInstance());
        serverURL = sharedPreferences.getString(MapSettings.KEY_layer_url, "");
    }

    public int addResultReceiver(ResultReceiver resultReceiver) {
        int index = resultReceivers.size();
        resultReceivers.put(index, resultReceiver);
        return index;
    }

    public void removeResultReceiver(int id) {
        try{
            resultReceivers.remove(id);
        }catch (Exception e){
            Log.d(TAG, "Tried to remove non existent ResultReceiver.");
        }
    }

    public List<MBTileDetails> getMergedList(){
        return availableDownloads;
    }

    private void sendResult(int resultCode, String data) {
        Bundle bundle = new Bundle();
        bundle.putString("data", data);
        Iterator<Map.Entry<Integer, ResultReceiver>> it = this.resultReceivers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, ResultReceiver> pairs = it.next();
            pairs.getValue().send(resultCode, bundle);
        }
    }

    public void sendUpdateListRequest() {
        AsyncHttpClient client = new AsyncHttpClient();
        Log.i(TAG, "Sending list request");
        client.get(serverURL, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
                sendResult(RESULT_LIST_UPDATE_FAILED, "");
                Log.i(TAG, "List request failed");
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
                String str = new String(arg2);
                final Type type = new TypeToken<List<MBTileDetails>>() {
                }.getType();
                Gson gson = new Gson();
                availableDownloads = gson.fromJson(str, type);
                sendResult(RESULT_LIST_UPDATE_SUCCEEDED, str);
                Log.i(TAG, "List request finished");
            }

        });
    }

    public void download(List<MBTileDetails> itemsToDownload) {
        for (MBTileDetails item:itemsToDownload){
            MBTileDownloader mtd = new MBTileDownloader(item, this);
            downloaders.add(mtd);
            mtd.download();
        }
    }

    public void cancelDownload(MBTileDetails downloadDetails) {
        for (int i = 0; i < downloaders.size(); i++) {
            MBTileDownloader mtd = downloaders.get(i);
            if(mtd.getDownloadID()==downloadDetails.getID()){
                mtd.cancelDownload();
                downloaders.remove(i);
                return;
            }
        }
    }

    class MBTileDownloader {
        private static final String TAG = "MBTileDownloader";
        private static final String MBTILES_EXTENSION = ".mbtiles";

        private MBTileDownloadManager manager = null;
        private MBTileDetails downloadDetails = null;
        private RequestHandle requestHandle = null;

        public MBTileDownloader(MBTileDetails downloadDetails, MBTileDownloadManager manager){
            this.downloadDetails = downloadDetails;
            this.manager = manager;
        }

        public int getDownloadID(){
            return downloadDetails.getID();
        }

        public void download() {
            Toast.makeText(Collect.getInstance(), "Downloading: " + downloadDetails.getName(), Toast.LENGTH_SHORT).show();
            AsyncHttpClient client = new AsyncHttpClient();
            Context ctx = Collect.getInstance();
            manager.sendResult(RESULT_LAYER_UPDATE_STARTED, Integer.toString(MBTileDownloader.this.getDownloadID()));
            requestHandle = client.get(downloadDetails.getUrl(), new FileAsyncHttpResponseHandler(Collect.getInstance()) {

                @Override
                public void onFailure(int arg0, Header[] arg1, Throwable arg2, File arg3) {
                    manager.sendResult(RESULT_LAYER_UPDATE_FAILED, Integer.toString(MBTileDownloader.this.getDownloadID()));
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, File arg2) {
                    Log.i(TAG, arg2.getAbsolutePath());
                    moveToOfflineLayersFolder(arg2);
                    manager.sendResult(RESULT_LAYER_UPDATE_SUCCEEDED, Integer.toString(MBTileDownloader.this.getDownloadID()));
                }

                @Override
                public void onProgress(int bytesWritten, int totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    Integer progress = (int)Math.floor(bytesWritten*100/totalSize);
                    String dataString = Integer.toString(MBTileDownloader.this.getDownloadID()) + ";" + progress.toString();
                    dataString += ";" + bytesWritten + ";" + totalSize;
                    manager.sendResult(RESULT_LAYER_UPDATE_PROGRESS, dataString);
                }
            });
        }

        private void moveToOfflineLayersFolder(File file){
            String directory = Collect.OFFLINE_LAYERS + File.separator + downloadDetails.getName();
            FileUtils.createFolder(directory);
            FileUtils.copyFile(file, new File(directory + File.separator + downloadDetails.getName() + MBTILES_EXTENSION));
            FileUtils.deleteAndReport(file);
        }

        public void cancelDownload(){
            if (requestHandle!=null){
                requestHandle.cancel(true);
                manager.sendResult(RESULT_LAYER_UPDATE_CANCELED, Integer.toString(MBTileDownloader.this.getDownloadID()));
            }
        }

    }
}
