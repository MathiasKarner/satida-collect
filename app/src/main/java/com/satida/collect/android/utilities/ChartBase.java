
package com.satida.collect.android.utilities;

import android.support.v4.app.FragmentActivity;

import com.satida.collect.android.R;


/**
 * Based on Philip Jahoda's ChartBase
 * Baseclass of all Activities of the Demo Application.
 * 
 * @author Philipp Jahoda, Mathias Karner
 */
public abstract class ChartBase extends FragmentActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
