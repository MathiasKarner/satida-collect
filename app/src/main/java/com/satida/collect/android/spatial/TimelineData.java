package com.satida.collect.android.spatial;

import android.graphics.Color;
import android.util.Log;

import com.github.mikephil.charting.components.YAxis;
import com.satida.collect.android.utilities.FileUtils;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by karner on 17.03.2015.
 */
public class TimelineData {

    private Integer[] count;
    private Double[] values;
    private Double[] avg;
    private Double[] stddev;
    private Double[][] warningthresholds;
    private Double[] forecastValue;
    private Double[] forecastProbability;

    private Boolean hasForecast = false;

    public Integer[] getCount() {
        return count;
    }
    public Double[] getValues() {
        return values;
    }
    public Double[] getAvg() {
        return avg;
    }
    public Double[] getStddev() {
        return stddev;
    }
    
    public TimelineData(){}
    
    public TimelineData(File jsonfile){
        Gson gson = new Gson();
        String json = new String(FileUtils.getFileAsBytes(jsonfile));
        TimelineData data = gson.fromJson(json, TimelineData.class);
        this.count = data.getCount();
        this.values = data.getValues();
        this.avg = data.getAvg();
        this.stddev = data.getStddev();
        this.calculateWarningThresholds();
    }

    public void calculateWarningThresholds(){
        warningthresholds = new Double[4][values.length];
        for (int i = 0; i <avg.length; i++) {
            Double average = avg[i];
            Double stddeviation = stddev[i];
            if ((average == null)||(stddeviation == null)){
                warningthresholds[0][i] = null;
                warningthresholds[1][i] = null;
                warningthresholds[2][i] = null;
                warningthresholds[3][i] = null;
                continue;
            }
            warningthresholds[0][i] = avg[i] - 0.5 * stddeviation;
            warningthresholds[1][i] = warningthresholds[0][i] - stddeviation;
            warningthresholds[2][i] = warningthresholds[1][i] - stddeviation;
            warningthresholds[3][i] = warningthresholds[2][i] - 300*stddeviation;
        }
    }

    public LineDataSet getValuesDatasetColored(){
        ArrayList<Entry> vals = new ArrayList<Entry>();
        ArrayList<Integer> colors = new ArrayList<Integer>();
        int[] col = {Color.RED,Color.GREEN, Color.BLUE};

        for (int i = 0; i < values.length; i++) {
            if (values[i]!=null){
                vals.add(new Entry(values[i].floatValue(), i+1));
                colors.add(col[i%3]);
            }
        }

        LineDataSet d1 = new LineDataSet(vals, "CDI Values");
        d1.setLineWidth(2.5f);
        d1.setColors(colors);
        d1.setCircleColors(colors);


        return d1;

    }

    public BarDataSet getBarDataset(){
        ArrayList<BarEntry> vals = new ArrayList<BarEntry>();

        for (int i = 0; i < values.length; i++) {
            if (values[i]!=null){
                vals.add(new BarEntry(values[i].floatValue(), i+1));
            }
        }

        BarDataSet d1 = new BarDataSet(vals, "CDI Values");
        d1.setColor(Color.BLACK);
        d1.setAxisDependency(YAxis.AxisDependency.LEFT);
        return d1;

    }

    public LineDataSet getValuesDataset(){
        ArrayList<Entry> vals = new ArrayList<Entry>();

        for (int i = 0; i < values.length; i++) {
            if (values[i]!=null){
                vals.add(new Entry(values[i].floatValue(), i+1));
            }
        }

        LineDataSet d = new LineDataSet(vals, "CDI Values");
        d.setLineWidth(2.5f);
        d.setCircleSize(4f);

        int color = Color.BLACK;
        d.setColor(color);
        d.setCircleColor(color);

        return d;
    }

    public LineDataSet getAverageDataset(){
        ArrayList<Entry> vals = new ArrayList<Entry>();

        for (int i = 0; i < values.length; i++) {
            if (avg[i%avg.length]!=null) {
                Entry e = new Entry(0,0);
                vals.add(new Entry(avg[i % avg.length].floatValue(), i + 1));
            }
        }

        LineDataSet d = new LineDataSet(vals, "CDI Averages");
        d.setLineWidth(2.5f);
        d.setCircleSize(4f);

        int color = Color.BLUE;
        d.setColor(color);
        d.setCircleColor(color);

        return d;
    }

    public List<LineDataSet> getWarningThresholds(Boolean colored){
        ArrayList<Entry> vals1 = new ArrayList<Entry>();
        ArrayList<Entry> vals2 = new ArrayList<Entry>();
        ArrayList<Entry> vals3 = new ArrayList<Entry>();
        ArrayList<Entry> vals4 = new ArrayList<Entry>();

        for (int i = 0; i < values.length; i++) {
            if (avg[i%avg.length]!=null)
                vals1.add(new Entry(warningthresholds[0][i%avg.length].floatValue(), i+1));
            if (warningthresholds[0][i%avg.length]!=null)
                vals2.add(new Entry(warningthresholds[1][i%avg.length].floatValue(), i+1));
            if (warningthresholds[1][i%avg.length]!=null)
                vals3.add(new Entry(warningthresholds[2][i%avg.length].floatValue(), i+1));
            if (warningthresholds[2][i%avg.length]!=null)
                vals4.add(new Entry(warningthresholds[3][i%avg.length].floatValue(), i+1));
        }

        LineDataSet d1 = new LineDataSet(vals1, "Normal Level");
        d1.setDrawCubic(true);
        d1.setCubicIntensity(0.2f);
        d1.setDrawValues(false);
        d1.setDrawCircles(false);
        d1.setLineWidth(2.5f);

        LineDataSet d2 = new LineDataSet(vals2, "Warning Level 1");
        d2.setDrawCubic(true);
        d2.setCubicIntensity(0.2f);
        d2.setDrawValues(false);
        d2.setDrawCircles(false);
        d2.setLineWidth(2.5f);

        LineDataSet d3 = new LineDataSet(vals3, "Warning Level 2");
        d3.setDrawCubic(true);
        d3.setCubicIntensity(0.2f);
        d3.setDrawValues(false);
        d3.setDrawCircles(false);
        d3.setLineWidth(2.5f);

        LineDataSet d4 = new LineDataSet(vals4, "Warning Level 3");
        d4.setDrawCubic(true);
        d4.setCubicIntensity(0.2f);
        d4.setDrawValues(false);
        d4.setDrawCircles(false);
        d4.setLineWidth(2.5f);

        if (colored){
            d1.setDrawFilled(true);
            int color = Color.GREEN;
            d1.setColor(color);
            d1.setFillColor(color);

            d2.setDrawFilled(true);
            color = Color.YELLOW;
            d2.setColor(color);
            d2.setFillColor(color);

            d3.setDrawFilled(true);
            color = Color.rgb(255, 102, 0);
            d3.setColor(color);
            d3.setFillColor(color);

            d4.setDrawFilled(true);
            color = Color.RED;
            d4.setColor(color);
            d4.setFillColor(color);
        }else{
            d1.setDrawFilled(false);
            int color = Color.GREEN;
            d1.setColor(color);

            d2.setDrawFilled(false);
            color = Color.YELLOW;
            d2.setColor(color);

            d3.setDrawFilled(false);
            color = Color.rgb(255, 102, 0);
            d3.setColor(color);

            d4.setDrawFilled(false);
            color = Color.RED;
            d4.setColor(color);

        }

        LineDataSet[] ds = {d4,d3,d2,d1};

        return Arrays.asList(ds);
    }

    public int getCountValues() {
        return values.length;
    }

    public boolean isEmpty(){
        int sum = 0;
        for (int i : count) sum += i;
        return (sum == 0);
    }

    public void normalizeWarningLevels() {

        for (int i = 0; i < values.length; i++) {
            if ((values[i]!=null)&&(avg[i%avg.length]!=null)&&(stddev[i%stddev.length]!=null)) {
                Double avg_t = avg[i % avg.length];
                Double sdv_t = stddev[i % stddev.length];
                values[i] = Math.max(0, ((0 - (values[i] - avg_t)) / sdv_t));
            }
            else
                values[i] = null;
        }

        for (int i = 0; i <avg.length; i++) {
            avg[i] = 0d;
            warningthresholds[0][i] = 0.5;
            warningthresholds[1][i] = 1.5;
            warningthresholds[2][i] = 2.5;
            warningthresholds[3][i] = 5.0;
        }
    }

    public void loadForecast(Double latitude, Double longitude, ForecastData forecastData, TimelineDataset tds) {
        if (forecastData == null) return;
        Integer[] pos = tds.latlng2pos(latitude, longitude);
        if (pos==null) return;
        hasForecast = true;
        try {
            this.forecastValue = forecastData.getForecast(pos[0], pos[1]);
            this.forecastProbability = forecastData.getProbabilities(pos[0], pos[1]);
        }catch (Exception e){
            hasForecast = false;
        }//I admit it, I'm a coward
    }
}
