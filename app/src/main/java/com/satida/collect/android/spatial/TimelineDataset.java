package com.satida.collect.android.spatial;

/**
 * Created by Mathias Karner
 * on 16.04.2015.
 */
public class TimelineDataset{

    private double geospatial_lat_min = 0;
    private double geospatial_lat_max = 0;
    private double geospatial_lon_min = 0;
    private double geospatial_lon_max = 0;

    private double pixelLengthLat = 0;
    private double pixelLengthLon = 0;

    private int shapeLat = 0;
    private int shapeLon = 0;

    private String[] dates = null;

    public TimelineDataset(){}

    public double getGeospatialLatMin() {
        return geospatial_lat_min;
    }
    public double getGeospatialLatMax() {
        return geospatial_lat_max;
    }
    public double getGeospatialLonMin() {
        return geospatial_lon_min;
    }
    public double getGeospatialLonMax() {
        return geospatial_lon_max;
    }

    public double getPixelLengthLat() {
        return pixelLengthLat;
    }
    public double getPixelLengthLon() {
        return pixelLengthLon;
    }

    public int getShapeLat() {
        return shapeLat;
    }
    public int getShapeLon() {
        return shapeLon;
    }

    public String[] getDates() {
        return dates;
    }

    public void fixMinMax(){
        if (geospatial_lat_max < geospatial_lat_min){
            double t = geospatial_lat_max;
            geospatial_lat_max = geospatial_lat_min;
            geospatial_lat_min = t;
        }
        if (geospatial_lon_max < geospatial_lon_min){
            double t = geospatial_lon_max;
            geospatial_lon_max = geospatial_lon_min;
            geospatial_lon_min = t;
        }
    }

    private Boolean isInside(Double latitude, Double longitude){
        if (latitude < this.geospatial_lat_min) return false;
        if (latitude > this.geospatial_lat_max) return false;
        if (longitude < this.geospatial_lon_min) return false;
        if (longitude > this.geospatial_lon_max) return false;
        return true;
    }

    public String getFilename(Double latitude, Double longitude){
        Integer[] pos = latlng2pos(latitude, longitude);
        if (pos==null) return null;
        return pos[0] + "-" + pos[1] + ".json";
    }

    public Integer[] latlng2pos(Double latitude, Double longitude){
        if (!this.isInside(latitude,longitude)) return null;
        int posLat = ((Double)Math.floor((latitude - geospatial_lat_min)/pixelLengthLat)).intValue();
        posLat = (-posLat) + shapeLat;
        int posLon = ((Double)Math.floor((longitude - geospatial_lon_min)/pixelLengthLon)).intValue();
        Integer[] pos = {posLat,posLon};
        return pos;
    }
}