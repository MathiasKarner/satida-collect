package com.satida.collect.android.spatial;

public class MBTileDetails {

    private Integer id;
    private String name;
    private String localName;
    private String url;
    private String timelines;
    private String forecast;
    private Long size;
    private String date;
    private String localDate;
    private Integer version;
    private Integer localVersion;
    private boolean download;

    public MBTileDetails() {
        download = false;
    }

    public Integer getID() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getUrl() {
        return url;
    }
    public String getTimelineUrl() {
        return timelines;
    }
    public String getForecastUrl() {
        return forecast;
    }
    public Long getSize() {
        return size;
    }
    public String getDate() {
        return date;
    }
    public Integer getVersion() {
        return version;
    }
    public Boolean getDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }

    public boolean isSameDataset(MBTileDetails dataset){
        if (dataset.getID() == this.id) return true;
        return false;
    }

    public void refreshOnlineInfo(MBTileDetails dataset){
        this.name = dataset.getName();
        this.url = dataset.getUrl();
        this.size = dataset.getSize();
        this.date = dataset.getDate();
        this.version = dataset.getVersion();
    }

    public void downloadFinishedActions(){
        this.localName = this.name;
        this.localDate = this.date;
        this.localVersion = this.version;
    }

    public String getDownloadSizeHR(){
        if (this.size>1000000000)
            return (Math.ceil(this.size/10000000)/100) + " GB";
        if (this.size>1000000)
            return ((int)Math.ceil(this.size/1000000)) + " MB";
        if (this.size>1000)
            return ((int)Math.ceil(this.size/1000)) + " kB";
        return this.size + " B";
    }


}
